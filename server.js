var app = require('http').createServer(handler),
    io = require('socket.io').listen(app),
    parser = new require('xml2json'),
    fs = require('fs'),
    mysql = require('mysql'),
    connectionsArray = [],
    connection = mysql.createConnection({
    	host : 'localhost',
    	user: 'root',
    	password: 'joash',
    	database: 'nodejs',
    	port: 3306
    }),
    POLLING_INTERVAL = 3000,
    pollingTimer;
    //f thereis an error connecting to the database
    connection.connect(function(err){
    	// conected! unless err is set
    	console.log(err); 
    });
    

//creating a server (localhost:8000)
app.listen(8000);

// on  the started server we ca load our client.html page
function handler(req, res){
	fs.readFile(__dirname + '/client.html', function(err, data){
		if(err) {
			console.log(err);
			res.writeHead(500);
			return res.end('Error while loading client.html');
		}
		console.log('Reading client.html');
		res.writeHead(200);
		res.end(data);
	});
}
//create a new websocket to keep the content updated without any AJAX request
io.sockets.on('connection', function(socket){
	console.log(__dirname);
	//watching the xml
	fs.watch(__dirname + '/example.xml', function(curr, prev){
		//on file change we can read the new xml
		fs.readFile(__dirname + '/example.xml', function(err, data){
			if(err) throw err;
			//parsing the new xml data and converting into json file
			var json = parser.toJson(data, {object: true});
			//adding the time of the last update
			json.test.time = new Date();
			console.log(json);
			//send the new data to the client
			socket.volatile.emit('notification', json);
		});
	});
});
